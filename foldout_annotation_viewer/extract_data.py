import argparse
import os
from pathlib import Path
import json
import urllib.request
import cv2
import numpy as np
from typing import Dict, Tuple, List
import p_tqdm
import PySimpleGUI as sg
from time import sleep
import platform

# List of random color combinations to have different colors for different persons in the image
import tqdm

colors = np.random.randint(0, 255, [100, 3])


def url_to_image(url: str) -> np.ndarray:
	"""
	Gets a URL and returns an image as numpy array.

	param:
		url: str
			the URL to return

	return:
		numpy.ndarray: loaded image from a URL.
	"""
	resp = urllib.request.urlopen(url)
	image = np.asarray(bytearray(resp.read()), dtype="uint8")
	image = cv2.imdecode(image, cv2.IMREAD_COLOR)
	return image


def arg_parse():
	"""
	Reads the input when the script is performed and parses the input.

	return:
		argument_object: object with parsed arguments as members.
	"""
	parser = argparse.ArgumentParser()
	parser.add_argument("file", type=str, help="The path to the json file.")
	parser.add_argument("--scale", nargs=1, help="Scale to resize image for viewing")
	parser.add_argument("--detected-only", action="store_true", help="Only show detected frames")
	parser.add_argument("--debug-single-download", action="store_true")

	return parser.parse_args()


def extract_data(annotation: Dict) -> Tuple[Dict, str]:
	"""
	Takes an annotation and extracts the points and track ID. ellipses are transformed into points.

	param:
		annotation: dict
			provided dictionary containing the points and track ID.
	return:
		dict: dictionary containing the points from the annotation input dictionary.
	"""
	points = {}
	id = annotation["name"]
	for frame in list(annotation["frames"].keys()):
		points[frame] = []
		if "keypoint" in annotation["frames"][frame]:
			x = int(np.round(annotation["frames"][frame]["keypoint"]["x"]))
			y = int(np.round(annotation["frames"][frame]["keypoint"]["y"]))
			points[frame].append({"x": x, "y": y})
		elif "ellipse" in annotation["frames"][frame]:
			id = id[:-1]
			center = (int(annotation["frames"][frame]["ellipse"]["center"]["x"]),
					  int(annotation["frames"][frame]["ellipse"]["center"]["y"]))
			radius = (int(annotation["frames"][frame]["ellipse"]["radius"]["x"]),
					  int(annotation["frames"][frame]["ellipse"]["radius"]["y"]))
			angle = annotation["frames"][frame]["ellipse"]["angle"] / np.pi * 180
			w = int(center[0] + np.max(radius) + 10)
			h = int(center[1] + np.max(radius) + 10)
			img = np.zeros((h, w), np.uint8)
			img = cv2.ellipse(img, center, radius, angle, 0, 360, color=[255], thickness=-1)
			p = np.where(img > 0)
			for yx in (zip(p[0], p[1])):
				(y, x) = yx
				points[frame].append({"x": x, "y": y})
	return points, id


def extract_detections(data: Dict) -> Dict:
	"""
	Extracts all the data (frames and containing points and track IDs) from a given json dictionary.

	param:
		data: dict
			Provided dictionary containing all the information of objects (points and track ID) as well as some meta
			data from a video file.

	return:
		dict: dictionary containing the points and track IDs from the annotation input dictionary for each frame with
		data.
	"""
	detections = {}
	for i in range(len(data['annotations'])):
		points, id = extract_data(data['annotations'][i])
		for frame in points:
			if frame not in detections:
				detections[frame] = {}
			if id not in detections[frame]:
				detections[frame][id] = []
			for point in points[frame]:
				detections[frame][id].append(point)
	return detections


def cache_images_on_disk(data: Dict, cache_directory: str, single_threaded_download: bool = False) -> List:
	"""
	Download all of the images into temporary cache directory

	param:
		data: dict
			Provided dictionary containing all the information of objects (points and track ID) as well as some meta
			data from a video file.
		cache_directory: str
			Cache directory

	return:
		list: dictionary of cached filenames of images
	"""

	image_urls = data["image"]["frame_urls"]

	def save_file(i: int, image_url: str, cache_directory: str) -> str:
		tmp_name = os.path.join(cache_directory, "{:05}.png".format(i))

		if not os.path.exists(tmp_name):
			image = url_to_image(image_url)
			cv2.imwrite(tmp_name, image)

		return tmp_name

	print("Downloading / checking cache [ {} ]:".format(cache_directory))

	if not single_threaded_download:
		output_filenames = p_tqdm.p_map(lambda x: save_file(x, image_urls[x], cache_directory), range(len(image_urls)))
	else:
		output_filenames = []

		for x in tqdm.tqdm(range(len(image_urls))):
			output_filenames.append(save_file(x, image_urls[x], cache_directory))

	return output_filenames


def show_points_on_images(cached_images, detections, scale=1, detections_only=False):
	"""
	For each image where data was provided the annotated points are shown with color coded track IDs.

	param:
		cached_images: list
			ordered list containing cached downloaded images
		detections: dict
			dictionary containing the points and track IDs from the annotation input dictionary for each frame with
			data.
		scale: int
			scale the final output image
	"""

	play_symbol = "▶️"
	stop_symbol = "⏹️"
	max_frames = len(detections) if detections_only else len(cached_images)

	layout = [
		[sg.Text("Showing Detected Frames Only" if detections_only else "Showing All Frames")],
		[sg.Button(button_text=stop_symbol, key="-play-", enable_events=True),
		 sg.Button(button_text="< Previous Frame", key="-previous-", enable_events=True),
		 sg.Button(button_text="> Next Frame", key="-next-", enable_events=True)],
		[sg.Image(key="-image-")],
		[sg.Slider(range=(0, max_frames), key="-slider-", orientation="h")]
	]

	window = sg.Window("FOLDOUT: Annotation Viewer", layout=layout, resizable=True, return_keyboard_events=True)

	elem_image = window["-image-"]
	elem_slide = window["-slider-"]
	elem_button = window["-play-"]

	play = True
	current_frame = 0
	frame_map = [frame for frame in detections]

	while True:
		event, values = window.read(timeout=0)
		update_frame = False

		if event is None:
			break

		if event == "-play-":
			if play:
				elem_button.update(play_symbol)
				play = False
			else:
				elem_button.update(stop_symbol)
				play = True

		if event == "-next-" or "Right:114" == event:
			if play:
				elem_button.update(play_symbol)
				play = False
			current_frame += 1
			update_frame = True
			elem_slide.update(current_frame)

		if event == "-previous-" or "Left:113" == event:
			if play:
				elem_button.update(play_symbol)
				play = False
			current_frame -= 1
			update_frame = True
			elem_slide.update(current_frame)

		if int(values['-slider-']) != current_frame and not update_frame:
			current_frame = int(values['-slider-'])
			update_frame = True

		if play:
			current_frame += 1
			update_frame = True

		if current_frame >= max_frames:
			elem_button.update(stop_symbol)
			play = False
			current_frame = max_frames - 1

		if current_frame < 0:
			current_frame = 0

		if update_frame:
			if detections_only:
				frame = frame_map[current_frame]
			else:
				frame = current_frame

			img = cv2.imread(cached_images[int(frame)])

			if str(frame) in detections:
				for track in detections[str(frame)]:
					color = colors[int(track.split(" ")[-1])].tolist()
					for point in detections[str(frame)][track]:
						center = (int(point["x"]), int(point["y"]))
						img = cv2.circle(img, center, 1, color, -1)

			desired = (int(img.shape[1] * float(scale)), int(img.shape[0] * float(scale)))

			if scale != 1:
				img = cv2.resize(img, desired)

			image_bytes = cv2.imencode('.png', img)[1].tobytes()

			elem_slide.update(current_frame)
			elem_image.update(data=image_bytes, size=desired)

		sleep(1/30)


def main():
	"""
	The main function which extracts the data from the json file and executes the extract_detections method and
	show_points_on_images.
	"""
	args = arg_parse()
	path_to_file = Path(args.file)

	if args.debug_single_download:
		print("===== DEBUG OPTION: Single Threaded Download =====")

	# Create temporary directory to cache images
	cache_dir_name = ".cache/{}".format(os.path.splitext(path_to_file.name)[0])

	cache_dir_exists = False

	if os.path.exists(cache_dir_name):
		print("Cache dir [ {} ] already exists. If it shouldn't, please delete manually.".format(cache_dir_name))
		cache_dir_exists = True

	os.makedirs(cache_dir_name, exist_ok=True)


	with open(path_to_file.absolute()) as json_file:
		data = json.load(json_file)

	# Cache images on disk
	cached_files = cache_images_on_disk(data, cache_dir_name, single_threaded_download=True if args.debug_single_download or cache_dir_exists else False)

	detections = extract_detections(data)
	detections_only = True if args.detected_only else False

	if not args.scale:
		show_points_on_images(cached_files, detections, detections_only=detections_only)
	else:
		show_points_on_images(cached_files, detections, args.scale[0], detections_only=detections_only)


if __name__ == '__main__':
	main()
